# Idling Roller Ends

![Hardware Photo](RollerEndB.png)

![Hardware Photo](RollerEndF.png)

These are a pair of idling roller ends that are designed to be used in conjunction with the [M8Mnt90 Bracket](https://github.com/AndyKirby/PrintableParts/tree/master/Mnt90) for a range of machine builds that need idling rollers to guide or convey materials. The ends take an M8 bore 608ZZ (Skate) bearing and were sized push fit into chromed curtain pole from a DIY store. Two types were created.

![CAD Photo](CADRollerEndFln.png)

A flange ended roller is intended to be used as is, or together with a plain ended roller. The flange helps to keep fed plastic film or other materials centred within the guided path.

![CAD Photo](CADRollerEndPln.png)

A plain ended roller is intended to be used as is, or together with a flanged roller. Fitting the plain ends just within the flange of the flanged roller as per the photos above is useful to position roll fed materials into the guided path and to ensure that when tensioned the film or material remains horizontal on the guided path.
