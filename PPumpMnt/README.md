# A Peristaltic Pump Mounting Bracket

![Hardware Photo](PPumpMnt.png)

This is a bracket for mounting a stepper driven peristaltic pump to M8 slotted, extruded aluminium machine framing. This was designed for precision dispensing of coatings via disposable dispensing nozzles. If I were doing this again I would design and build the pump as well as the mounting. There were a few issues with the pump, that was bought in, and the application, which disappointed. Off the shelf pumps are quite expensive but eminently printable.

![CAD Photo](CAMPPumpMnt.png)
