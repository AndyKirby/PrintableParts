Picroscope

The Picroscope can be built as two variants, both are based on a PiZero and the High Quality Raspicam, and use the same software etc. But the optics, illumination and stand vary to fit the useage.


Picroscope LF

The Picroscope LF (Long Focus) is built around a long focal length (zoom type) lens and is suitable for electronic repair, modification, inspection work and soldering of fine components and traces. The HDMI video display (use any HDMI monitor with the PiZero) is preferable to a web based display as it has no has no lag. Lag can impact hand eye coordination and make fine soldering work impossible for most users. The Picroscope LF uses an illuminator attached to the front of the lens and can selectively illuminate the workpiecee with Wwhite, Red, Green or Blue light. The attached 43mm polarising lense in conjunction with selective illumination is intended to assist in resolving difficult to read IC's and identtification markings.

Components:-
Pi Zero W
Raspberry Pi High Quality camera with C/CS mount
Printable Pi Zero Camera Case
NeoPixel Ring - 24 x 5050 RGBW LED's (eg https://shop.pimoroni.com/products/neopixel-ring-24-x-5050-rgbw-leds-w-integrated-drivers?variant=17450439431)
8X - 100X Adjustable Zoom C mount microscope lens (eg https://www.aliexpress.com/item/32843165158.html?gps-id=pcStoreJustForYou&scm=1007.23125.137358.0&scm_id=1007.23125.137358.0&scm-url=1007.23125.137358.0&pvid=94d93e4a-8f40-4f12-8a55-c62b1063c2ec&spm=a2g0o.store_home.smartJustForYou_126762550.1)
 

Picroscope SF

The Picroscope SF (Short Focus) is built around cheap replacement biology microscope objective lenses and is moreare suitable for biology, metalurgy and semiconductor die inspection work. These lenses have a very short focus and usualy require biology speciments to be mounted on glass slides and backlit. The HDMI video display (use any HDMI monitor with the PiZero) is preferable to a web based display as it has no has no lag. Lag can impact hand eye coordination and make fine manipulation impossible for most users.

Components :-
Pi Zero W
Raspberry Pi High Quality camera with C/CS mount
Printable Pi Zero Camera Case
Neopixel Illuminator
Microscope Objective Lens Achromatic (eg https://www.aliexpress.com/item/4000530982596.html?spm=2114.12010612.8148356.4.20e361e2oziCeY)



The printable Pi Zero camera case is an unmodified print of:-
https://www.thingiverse.com/thing:4338679

All the other printable parts are new designs.
