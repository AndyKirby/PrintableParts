/*
    Author: Andy Kirby
    Email: andy@kirbyand.co.uk
    WWW: http://www.kirbyand.co.uk
    Date: 20/05/2012
    Licensing: GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    Script to create a set of 3 feet to stand off the bottom of a coffee can 
    filter from the bench top

    The feet are fixed using one M3 by 35mm bolt or machine screw.

    Hexahon holes are provided in the base of the feet as captive nut holders.
    As well as recesses to put rubber stick on feet onto the bottom. 
*/


/* Global Variables */
// Change these to change the parts parameters
odn = 6.5;			// OD of M3 nut corner to corner mm
nthick = 3;			// Thickness of m3 nut
ods = 3;				// OD of M3 fixing screw mm
slength = 35;			// Screw Length
odf = 12;			// OD of stick on rubber foot mm
odbot = odf *1.5; 		// OD of bottom of foot mm
odtop = odbot * 1.5;	// OD of top of foot mm
fheight = 40;			// Height of foot mm
facets  = 32;        		// Smoothness of curves in faces
rbrdepth = 1;			// Recess to stick rubber foot in


/* Pre-Calculations */
// These are pre-calculations and should not be altered.
rn = odn / 2;			// radius of M3 nut corner to corner mm
rs = ods / 2;			// radius of M3 fixing screw mm
rf = odf / 2;			// radius of stick on rubber foot mm
rbot = odbot / 2;		// radius of bottom of foot mm
rtop = odtop / 2;		// radius of top of foot mm
cnchannel = fheight - slength + (nthick * 2);  //how long to make the captive nut channel


/* Main or start functon basicaly do it */
render (convexity = 1) {
  3feet();
}


/* 
   Module to create all three parts.
*/
module 3feet(){
// Uncomment as many as needed
   translate(v = [0,0,fheight / 2])   foot();
//   translate(v = [20,0,fheight / 2])  foot();
//   translate(v = [40,0,fheight / 2]) foot();
}


/* 
   Module to create a foot
*/
module foot(){
   difference () {
      basicfoot();
      bolthole();
      translate(v = [0,0,fheight / 2]) captnut();
      translate(v = [0,0,fheight / 2]) rbrrecess();
   }
}


/* 
   Module to create the top clamp
*/
module basicfoot(){
   cylinder(h=fheight, r1=rtop, r2=rbot, center=true, $fn = facets);
}


/*
 Module to create the bolt hole
*/
module bolthole(){
   cylinder(h=fheight * 2 , r=rs, center=true, $fn = facets);
}


/*
 Module to create the bolt hole
*/
module captnut(){
   cylinder(h=cnchannel * 2 , r=rn, center=true, $fn = 6 );
}


/*
 Module to create the recess for the stick on rubber foot
 Placing this in a recess stops it sliding off if whatever the
 foot is attached to is slid across a high friction surface.
*/
module  rbrrecess(){
   cylinder(h=rbrdepth * 2 , r=rf, center=true, $fn = facets );
}

