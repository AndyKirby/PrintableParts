# A set of feet for a coffee can air scrubber

![Hardware Photo](Filter.jpg)

3D Printing can create noxious gasses from melting plastics. This part is for making a set of feet for a coffee can air scrubber. The coffee can has holes punched in the bottom or a circle cut out and is filled with activated charcoal held between two sponge pads. A mains fan is re-purposed to pull the air through the charcoal, scrubbing it as it goes. Providing the fan is blowing air out of the top of the tin and is mounted to a thin support plate the original plastic sealing cap for the can will adequately seal the top.

The filter feet are necessary to make sure there is a clear airway underneath the coffee can. They have been designed for fixing to the can with long M3 bolts. The feet feature a captive nut socket and an indent to put sticky rubber pads on the bottom of the feet.

![Part Piccy](FilterFeet.jpg)
