# A Tray of Chopstick Trainers

![Hardware Photo](CST-Tray.jpg)

This is a tray of chopstick trainers for bulk printing. These are for freebies to give away at Maker Fair's and such like. The part is taken directly from [Zydac's Chopstick Trainer Part](http://www.thingiverse.com/thing:5923/#files). This was then imported to Freecad and copied to fill a reasonable amount of the build space on the average 3D printer. A tray full is printed as 24 pieces arranged in 4 columns of 6. There was little point in re-inventing this wheel, this is just to ease bulk production.
