# A Mounting Bracket for T Slot Aluminium Extrusion

![Hardware Photo](M8Mnt90.png)

M8Mnt90 was a mounting bracket for use with [conveyor rollers](https://github.com/AndyKirby/PrintableParts/tree/master/RollerEnds) and driven components when building machines with a frame made from aluminium extrusion with M8 T slots. The bracket clamps securely to the frame via the M8 mounting holes in the base and a locating pin (M8 Machine screw) is screwed through the bracket and the captive nut into the component that is suspended on the locating pin. This part is commonly used with a rolling component that uses an M8 bore 608ZZ (Skate) bearing as the suspension that the locating pin mates with.

![M8Mnt Photo](CADM8Mnt90.png)

A slightly smaller part featuring M6 mounting holes was derived from this for a secondary glazing project being carried out by [Studio Polpo](http://www.studiopolpo.com/) in conjunction with [Portland Works](http://www.portlandworks.co.uk/). This part screws to the frame of the secondary glazing and the locating pins are tightened up to secure the glazing within the window reveal.

![M6Mnt90 Photo](M6Mnt90.jpg)

I also created a parts layup to print the parts more than one at a time, as each secondary glazing unit needs 3 attachments.

![PrintSet Photo](PrintSet.jpg)

# Notes:-
* Nut traps are press fit so as to retain the nut when there is no locating pin present.
