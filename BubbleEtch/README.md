# DIY Bubble Etch Tank Parts

![Hardware Photo](BubbleTest.jpg)

A set of parts to use when making your own bubble etch tank out of a cheap plastic cereal container. The set comprises a handle and a pair of clamps to hold the PCB's in place. The handle has a central notch to help stabilise it when hanging it over a sink for washing down. The board clamps both have locating slots to space the PCB's to be etched and the lower clamp has holes to pass a perforated airline through. Holding the perforated airline stops it floating up and fouling the boards being etched. In this case the air line was driven by an inexpensive aquarium air pump. Earlier versions using an aquarium air stone failed as the etchant ate the air stone. The lid of the cereal container the handle and both clamps are fastened together with M8 nylon threaded rod and nylon nuts.

![Parts Set](BubbleEtch.jpg)
