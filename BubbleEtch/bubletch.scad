/*
    Author: Andy Kirby, Email: andy@kirbyand.co.uk, Date: 15/01/2011
    Licensing GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    script to create a pair of bubble etch board clamps and a handle.

    The lower clamp has two aditional holes to hold the loop of punctured
    bubbler pipe. Which is fed via a T piece from a suitably rated aquarium
    air pump.

   The Handle is shaped so that it prefers to ballance if hung over a rod or hook
   or line when draining or washing the etched boards.

   All the parts are threaded onto a pair of threaded nylon rods which
   clamp and fix together using nylon nuts. Nylon fixings are chemicaly
   resistant.
*/


/* Global Variables */
// Change these to change the parts parameters
odn = 16;             // OD of nylon nut corner to corner mm
ods = 8;	         // OD of nylon studding mm
odp = 5.5;               // OD-radius of air tube mm
facets  = 32;        // smoothness of curves in faces
centres = 40;       // spacing between centres of the studding
width =67;           // the overall width of the pieces
sw = 2;                 // slot width
sd = 2;                  // slot depth
numslots = 5;	// number of board slots required


/* Pre-Calculations */
// These are pre-calculations and should not be altered.
rn = odn / 2;                // radius of Nylon nutcorner to corner mm
rs = ods / 2;	      // radius of nylon studding mm
rp = odp / 2;               // OD-radius of air tube mm
bar = odn - 4;	     // make the depth of the bars just less than the pillars
rbar = bar / 2;             // radius of the bar.
slotcent = sw / 2;       // the centre of the board slot
slotzoff = rbar - slotcent; // Calc to get the board slots to be flush with bottom of bars
sloffset = width / numslots; // spacing between slot centres
hcent = centres/2;     // distance for centers from center origin
thick= bar;	         // piece minimum thickness mm



/* Main or start functon basicaly do it */
render (convexity = 1) {
  bubletch();
}


/* 
   Module to create all three parts.
*/
module bubletch(){

// Uncomment to do Bottom clamp
//   translate(v = [0,0,thick / 2])   botclamp();

// Uncomment to do Top clamp
//  translate(v = [0,0,thick / 2])  topclamp();

// Uncomment to do Handle
//  translate(v = [0,0,thick / 2])  handle();

// Uncomment to do all three together
   translate(v = [0,0,thick / 2])   botclamp();
   translate(v = [20,0,thick / 2])  topclamp();
   translate(v = [40,0,thick / 2])  handle();


}


/* 
   Module to create the bottom clamp
*/
module botclamp(){
   difference () {
      basicblock();
      boardslots();
      translate(v = [0,(hcent - ods),0])  rotate ([0,90,0])  cylinder(h=odn, r=rp, center = true,  $fn = facets);
      translate(v = [0,-(hcent - ods),0])  rotate ([0,90,0])  cylinder(h=odn, r=rp, center = true,  $fn = facets);
   }
}


/* 
   Module to create the top clamp
*/
module topclamp(){
   difference () {
      basicblock();
      boardslots();
   }
}


/* 
   Module to create the handle
*/
module handle(){
   difference () {
      basicblock();
      translate(v = [0,0,rbar])  rotate ([0,90,0])  cylinder(h=odn, r=3, center = true,  $fn = facets);
   }
}


/* 
   Module to create the common clamp/block that all the parts are based on
*/
module basicblock() {
   difference () {
      union () {
         intersection () {
            cube(size = [bar,width,thick], center = true);
           translate(v = [0,0,-rbar])  rotate ([90,0,0])  cylinder(h=width, r=thick, center = true,  $fn = facets);
         }
         translate(v = [0,- hcent,0])  cylinder(h=thick, r=rn, center=true, $fn = facets);
         translate(v = [0,hcent,0])  cylinder(h=thick, r=rn, center=true, $fn = facets);
      }
      translate(v = [0,- hcent,0])  cylinder(h=thick, r=rs, center=true, $fn = facets);
      translate(v = [0,hcent,0])  cylinder(h=thick, r=rs, center=true, $fn = facets);
   }
}


/*
   Module to create an array of objects to subtract from the bar and make board slots
*/
module boardslots() {
      translate(v = [0,0, - slotzoff]) cube(size = [odn,sw,sd], center = true);
      translate(v = [0,sloffset,- slotzoff]) cube(size = [odn,sw,sd], center = true);
      translate(v = [0,sloffset * 2,- slotzoff]) cube(size = [odn,sw,sd], center = true);
      translate(v = [0,- sloffset,- slotzoff]) cube(size = [odn,sw,sd], center = true);
      translate(v = [0,- sloffset *2,- slotzoff]) cube(size = [odn,sw,sd], center = true);
}
