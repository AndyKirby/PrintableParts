## Alles Case

A case/enclosure for a number of Alles distributed sysnth's I built.

[Brian Whitman's Alles Distributed Synth Project](https://github.com/bwhitman/alles)

The case is dimensioned for an ALCO CD189 (4"/101mm) speaker and the ALSA synth build is on a 50mm x 70mm proto board. There is a hole in the rear of the case to mount a DC barrel jack to power the synth.

![Photo finished cases](AllesCase.jpg)*Cases in asembled and ready to assemble form*

The design is intended to be printed from a solvent weldable plastic, with the case body solvent welded to the back panel. Some platics may prefer to be glued instead.

The case overall is suficiently discreet for a number to be distributed around a space or alternatively being a regular hexagon they can be stacked up into different shapes of synth walls.

[![CAD object, back panel](BackPanel.jpg)*CAD object, back panel*](BackPanel.stl)

[![CAD object, case body](Body.jpg)*CAD object, case body*](Body.stl)

[![CAD object, speaker retaining bezel](Bezel.jpg)*CAD object, speaker retaining bezel*](Bezel.stl)
