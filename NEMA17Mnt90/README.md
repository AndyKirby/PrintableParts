# A NEMA 17 Mounting Bracket

![Hardware Photo](NEMA17Mnt90.png)

This is a bracket for mounting NEMA 17 stepper motors to M8 slotted, extruded aluminium machine framing. Useful for implementing anything you need to use a stepper motor for when building machines. The photo shows one being used to direct drive a carriage assembly via a timing belt.

![CAD Photo](CADNEMA17Mnt90.png)
