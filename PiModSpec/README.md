# A modular pi zero based optical spectrometer

This is a sequence of parts to make a modular Pi Zero based optical spectrometer. 
The internal mounting slots are sized to take a standard 35MM photographic slide. 
Easily interchangeable modules are based on this for mounting in the slots. Making the spectrometer quickly reconfigurable between experiments.


# Notes:-
* Designed to be printed in black opaque ABS or PLA specificaly to exclude stray light.
* line inisides and baffles with bacl optical flock if the printed plastic is too shiny.
* Optical SMA connectors are mind bogglingly expensive use standard RF ones and push out the internals.
* Secure the Pi Zero to the relevant cover with M2 hardware
