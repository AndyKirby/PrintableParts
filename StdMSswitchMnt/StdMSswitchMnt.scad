/*
    Author: Andy Kirby, Email: andy@kirbyand.co.uk, Date: 18/03/2012
    Licensing GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    Script to create a mount for a standard microswitch on M8 Rod.
    Mainly to be used on RepRap Machinery as a homing switch.
*/


/* Global Variables */
// Change these to change the parts parameters
lside=28;	// Long Side length
sside=16;	// Short Side length
x_crnr_offset = 11.1; // x hole centre for bolt hole and corner radius
y_crnr_offset = 5.15; // y hole centre for bolt hole and corner radius
drod=8.0;		// Diameter of rod we are fixing the switch to
dbolthole=3.0;	// Diameter of the bolt Holes
smoothness=32;	// number of facets to use on curves
cradius=2.8;	// Corner Radius for main block


/* Pre-Calculations */
// These are pre-calculations and should not be altered.
blockthick=2*drod;	//thickness of the mount set at twice the rod it is on.
rrod=drod/2.0;		// radius of the rod.
rbolthole=dbolthole/2.0; // radius of the bolt holes
alignz=blockthick/2;	//How much to lift the piece to sit it on the print bed.


/* Main or start functon basicaly do it */
render (convexity = 1) {

  // uncomment this one to get a plan view exportable to dxf
  //projection(cut = false) translate(v = [0, 0, alignz+2]) mswitch_mnt();

  // uncomment this one to get an object exportable to stl
  translate(v = [0, 0, alignz]) mswitch_mnt();


}


/*
   Module create the object by subtracting the elements from a master block
*/
module mswitch_mnt() {
   difference() {
      translate(v = [0, 0, -alignz])mntblock();
      translate(v = [0, 0, alignz/2])rotate ([90,0,0]) rodslot();
      translate(v = [x_crnr_offset, y_crnr_offset, 0]) bolthole();
      translate(v = [-x_crnr_offset, -y_crnr_offset, 0]) bolthole();
   }
}


/* 
   Module to create the master block for the mounting
*/
module mntblock(){
   
   linear_extrude(height=blockthick) hull() {
      // place 4 circles in the corners, with the given radius
      translate([x_crnr_offset, y_crnr_offset, 0]) circle(r=cradius, $fn=smoothness);
      translate([x_crnr_offset, -y_crnr_offset, 0]) circle(r=cradius, $fn=smoothness);
      translate([-x_crnr_offset, y_crnr_offset, 0]) circle(r=cradius, $fn=smoothness);
      translate([-x_crnr_offset, -y_crnr_offset, 0]) circle(r=cradius, $fn=smoothness);
   }
}


/*
   Module to create the shape to subtract and create a rod slot
*/
module rodslot(){
   union(){
      translate(v = [0, rrod, 0]) cube(size = [drod, drod, (2 * sside)],center=true);
      cylinder(h=(2 * sside), r=rrod, center=true, $fn=smoothness);
   }
}


/*
   Module to create the shape to subtract and create a bolt hole
*/
module bolthole(){
   cylinder(h = 2 * blockthick, r = rbolthole, center = true, $fn=smoothness);
}
