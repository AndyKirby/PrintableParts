# Standard Micro-Switch Mount

![CAD Photo](VacStrainer.jpg)

A quick and dirty design to mount an end-stop switch onto the Y axis rails for a Reprap Huxley 3D printer. Not the nest design as it uses the micro-switch body as one of the clamp faces. It also needs a piece of rubber or some such sandwiching between the switch and rail to make sure it does not creep along the rail over time. But it worked.
