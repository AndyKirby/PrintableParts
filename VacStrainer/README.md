# Vacuum Strainer

![Hardware Photo](FittedStrainer.jpg)

A design for a strainer to fit the dust brush attachment of a hoover. So that on the rare ocasions I hoover in my workspace it prevents small parts and notes being taken away by the vacuum cleaner. It is sized to snugly drop into the openin inside the bristles and suction holds it there when in use. A trivial thing but a lifesaver all the same.

![CAD Photo](VacStrainer.jpg)
