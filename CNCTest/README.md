# CNC test piece

![Hardware Photo](CNCTest.jpg)

This is a CNC Router/Milling/Printing test piece. It comprises 3 stacked shapes square diamond and circle With an indented triangle showing the correct X/Y way up for the piece.
There is also a drill hole on the top with an ID of exactly 6mm and a depth of 5mm same as the triangle. 
The pieces are drawn stacked with an overall X,Y origin dead centre at 0,0.

Square is 40 x 40 mm
Diamond clears the squares straight edges on the cardinal points by exactly 2mm
Circle has an outside diameter of 22mm

Triangle has its base along the X axis at Y=0 and its apex pointing in the direction of +Y it is equilateral being 9mm per side

Thickness of each stacked part is exactly 5mm making the piece overall 15mm

Print/Mill/route the piece and check the critical dimensions and their relative sizes/position with digital/vernier callipers. Knowing which way around the piece was in the machine axis specific errors can be traced back.
