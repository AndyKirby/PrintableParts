difference() { 

translate([0,0,0])
cube ([120,52,10]);

translate([33.,26,-5])
cylinder(r=15, h=20);

translate([63,26,-5])
cylinder(r=5, h=20);

polyhedron ( points = [[-5, -5, -10], [-5, -5, 20], [-5, 20, -10], [-5, 20, 20], [40, -5, -10], [40, -5, 20]], 
triangles = [[1,0,3], [2,3,0], [0,1,4], [5,4,1], [4,5,3], [3,2,4], [1,3,5], [0,4,2] ]);

polyhedron ( points = [[-5, 57, -10], [5, 57, 20], [-5, 32, -10], [-5, 32, 20], [40, 57, -10], [40, 57, 20]], 
triangles = [[1,3,0], [3,2,0], [4,1,0], [5,1,4], [3,5,4], [2,3,4], [5,3,1], [2,4,0] ]);

translate([10,26,-5])
cylinder(r=5, h=20);

translate([108,8.96,-5])
cylinder(r=5, h=20);

translate([108,43.60,-5])
cylinder(r=5, h=20);

translate([108,4,-5])
cube ([17,10,17]);

translate([108,38.5,-5])
cube ([17,10,17]);

}




