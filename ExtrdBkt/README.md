# Extruder Bracket

![Hardware Photo](extrbkt.jpg)


Whilst ExtrBKT is living in my collection the actual credit for this piece goes to Aaron Ibotson.

Aaron is/was a very young member of a group which I ran that became [Sheffield Hardware Hackers and Makers](http://www.sheffieldhardwarehackers.org.uk/wordpress/). This was one of his first contributions to the world of 3D printing.

The piece is a mounting bracket to fix a Wade Extruder and Bowden Tube filament feed onto the top frame of my first 3D Printer a stretched Sells Huxley.

I later reworked this into Feeder Plate, but the original measuring and designing etc was AJ's work.

![Hardware Photo](feederplate.jpg)
