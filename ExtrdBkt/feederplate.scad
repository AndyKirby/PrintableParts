/*
    Author: Andy Kirby, Email: andy@kirbyand.co.uk, Date: 18/03/2012
    Licensing GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    Script to create a mount for a standard microswitch on M8 Rod.
    Mainly to be used on RepRap Machinery as a homing switch.
*/


/* Global Variables */
// Change these to change the parts parameters
pltwidth=54.0;	// Overall Width
lowerprt=25.0;	// Length of lower part
upperprt=95.0;	// Length of upper part
pltthick=5.0;		// Thickness of the mounting plate
smoothness=32;	// number of facets to use on curves
cradius=3.0;		// Corner Radius for main block
drod=6.0;		// Diameter of rod we are fixing the plate to
dbolthole=5.0;	// Diameter of the bolt Holes
dbwhole=18.0;	// Diameter fo hole to pass bowden wade adapter through
slotdepth=10.0;	// Required depth of the frame mounting slots
epwidth=28.0;	// Width of the extruder base plate
extbuoffset=22;	// Offset from centre of dbwhole for upper mounting hole
extbloffset=29;	// Offset from centre of dbwhole for lower mounting hole
extmntref=62;	// Extruder mounting offset to ref centre of dbwhole


/* Pre-Calculations */
// These are pre-calculations and should not be altered.
rrod=drod/2.0;						// Radius of the rod.
pltlength=lowerprt+upperprt;			// Overall length
rbolthole=dbolthole/2.0; 				// Radius of the bolt holes
alignz=pltthick/2.0;					// How much to lift the piece to sit it on the print bed.
halfwidth=pltwidth/2.0;				// The width of each half from the Y centre line (0)
lowerwrp=halfwidth-cradius;			// The lower halfwidth offset for the required radiusing
lowerlrp=lowerprt-cradius;				// The lower length offset for the required radusingd
slotoffset=lowerpart-slotdepth+cradius; 	// The offset needed to put the slot top in the ritgh place
epwradius=epwidth/2.0;				// The radius needed to create the top of the mounitng plate
rbwhole=dbwhole/2.0;					// The radius needed to create dbwhole


/* Main or start functon basicaly do it */
render (convexity = 1) {

  // uncomment this one to get a plan view exportable to dxf
  //projection(cut = false) translate(v = [0, 0, alignz+2]) feeder_mnt_plate();

  // uncomment this one to get an object as it comes, exportable as stl
  feeder_mnt_plate();

}


/*
   Module create the object by subtracting the elements from a master block
*/
module feeder_mnt_plate() {
   mntplt();
}


/* 
   Module to create the master block for the mounting
*/
module mntplt(){

   linear_extrude(height=pltthick) { 
      difference() {
         // Plate
         plateoutline();
         // two mounting slots
         translate([-(lowerwrp-drod),-(lowerprt-slotdepth)]) slotoutline();
         translate([(lowerwrp-drod),-(lowerprt-slotdepth)]) slotoutline();
         // extruder mounting holes
         translate([0,extmntref]) extrholes();
      }
   }   
}


/*
   Module to create plate outline with radiused corners
*/
module plateoutline(){
   hull() { 
      // Place two circles for radiused transition from lower part to upper part
      translate([-lowerwrp, 0, 0]) circle(r=cradius, $fn=smoothness);
      translate([+lowerwrp, 0, 0]) circle(r=cradius, $fn=smoothness);

      // Put down perimeter shapes for upper half
      translate([0, upperprt-epwradius, 0]) circle(r=epwradius, $fn=smoothness);     

      // Put down perimeter shapes for lower half
      translate([-lowerwrp, -lowerlrp, 0]) circle(r=cradius, $fn=smoothness);
      translate([+lowerwrp,-lowerlrp, 0]) circle(r=cradius, $fn=smoothness);
   }
}


/*
   Module to create the subtractable outline of a slot
*/
module slotoutline(){
   union(){
      circle(r=rrod, $fn=smoothness);
      translate([0,-(slotdepth/2)]) square([drod,slotdepth], center=true);
   }
}

/*
   Module to create the subtractable outlins of the holes needed to mount the extruder
*/
module extrholes(){
   circle(r=rbwhole, $fn=smoothness);
   translate([0,extbuoffset]) circle(r=rbolthole, $fn=smoothness);
   translate([0,-extbloffset]) circle(r=rbolthole, $fn=smoothness);
}
    
