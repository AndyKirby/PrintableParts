# A Bowden Wade Adapter

![Hardware Photo](wadebdnadpt.jpg)

The Wade extruder mechanism for a 3D printer is intended to connect directly to the stem of a hot end, being retained by a couple of pins. This adapter is intended to convert the Wade Extruder to drive the filament down a Bowden Tube. There is a printable version scripted in openscad and a picture of a similar piece turned on a lathe. The captive nut in the printable version visible un-captive in the turned version grips the PTFE Bowden Tube and stops the filament pressure pulling it out of the adapter.

![Printable Version](BowdenWadeAdapter.jpg)
