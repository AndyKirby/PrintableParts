/*
    Author: Andy Kirby, Email: andy@kirbyand.co.uk, Date: 27/12/2010
    Contributors: 
    Licensing GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    Script to create a plastic Bowden Tube to Wade extruder adapter.
*/

/* Global Variables */
// Parameters to change the parts vital dimensions
adapth = 20;                // Overall height of the adapter (mm)
bowdenod = 5;            // Outside Diameter of Bowden Tube (mm)
wadeid = 15;	         // Inside Diameter Extruder socket on Wade Extruder (mm)
nuth = 4;                     // Height of nut used to anchor bowden tube (mm)
nutw = 8;                    // Width of nut used to anchor bowden tube across adjacent flats (mm)
retd = 2.5;                     // Retainer groove depth
retw = 2;                    // Retainer groove width
retoffset = 4;            // Retainer Groove offset from wade end of adapter
smoothness = 32;      // Number of facets making up the circles in the piece


// Do not change precalculated factors.
nutchanh = adapth * 0.66;
nuty =  - (wadeid / 2);
nutx =  - (nutw / 2);
wadeir = wadeid / 2;
bowdenor = bowdenod / 2;
grooveir = (wadeid - retd) / 2;


/* Main or start function basicaly do it */
render (convexity = 1) {
   bwadapter();
}

/* The part description */
module bwadapter() {
   difference() {
      // from the adapter solid
        cylinder(h=adapth, r1=wadeir, r2=wadeir, center=false, $fn=smoothness);
      // subtract the hole 
        cylinder(h=adapth, r1=bowdenor, r2=bowdenor, center=false, $fn=smoothness);
      // subtract the nut channel
          translate(v = [nutx, nuty, nutchanh] ) {
             cube(size = [nutw, wadeid, nuth]);
          }   
      // subtract the retainer groove
         translate(v = [0, 0, retoffset] ) {
            difference() {
               cylinder(h=retw, r1=wadeir, r2=wadeir, center=false, $fn=smoothness);
               cylinder(h=retw, r1=grooveir, r2=grooveir, center=false, $fn=smoothness);
            }
         }
      // subtract the retainer groove corbel
         translate(v = [0, 0, retoffset + retw] ) {
            difference() {
               cylinder(h=retw, r1=wadeir, r2=wadeir, center=false, $fn=smoothness);
               cylinder(h=retw, r1=grooveir, r2=wadeir, center=false, $fn=smoothness);
            }
         }
   }
}
