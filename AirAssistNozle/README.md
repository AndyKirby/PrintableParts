# A Laser Cutter Air Assist Nozzle

[![CAD object](AirAssistNozle.jpg)*CAD image, air assist nozzle body*](AirAssistNozzle.stl)

An air assist nozzle for Sheffield Hackspace's inexpensive Chinese laser cutter. The laser cuter is a very basic model and came without air assist. This is a simple design to add the capability. It is designed to slip on the existing lens assembly and can be secured with a small dab of rubber cement. Nothing stronger should be used as you may want to take it all apart later. The glue just stops it dropping off or shifting off axis mid cut.

To finish assembly glue a trimmed No5 Icing Nozzle into the bottom rebate and thread/screw a quick release pneumatic airline connector to the air port on the top.

The metal icing nozzle may get hot and pritning this item from low teperature plastics like PLA (except as an investment cast) is to be avoided.
