/*
    Author: Andy Kirby, Email: andy@kirbyand.co.uk, Date: 27/12/2010
    Contributors: 
    Licensing GPL V3 (http://www.gnu.org/licenses/quick-guide-gplv3.html).

    Script to create a plastic spacer, thin spacers can also be used as washers.
*/

/* Global Variables */
// Parameters to change the parts vital dimensions
spacerh = 10;                // overall height of the spacer (mm)
holeid = 6;                   // Inside Diameter of centre hole (mm)
spacerod = 10;	         // Outside Diameter of spacer
smoothness = 32;      // Number of facets making up the circles in the piece


// Do not change precalculated factors.
holer = holeid / 2;
spaceror = spacerod / 2;


/* Main or start function basicaly do it */
render (convexity = 1) {
   spacer();
}

/* The part description */
module spacer() {
   difference() {
     // from the spacer solid
        cylinder(h=spacerh, r1=spaceror, r2=spaceror, center=false, $fn=smoothness);
     // subtract the hole 
        cylinder(h=spacerh, r1=holer, r2=holer, center=false, $fn=smoothness);
   }
}
