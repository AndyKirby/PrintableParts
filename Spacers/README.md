# Random Spacers

A pair of scripts for generating random plastic spacers for printing. Those anoying things that you never have in either the right size or number when you most need them for mounting projects. Together with some printable files for spacer sizes comonly used and left over from other projects. A plastic washer is just a thin wide spacer. A poor plastic nut can be made by sizing a hex spacer correctly and tapping the hole to suit. Open the relevant script in openscad, edit the parameters at the top to specify your spacer then generate and export it.

![CAD Photo](HexSpacer.jpg)

A hex spacer script.

![CAD Photo](Spacer.jpg)

A plain round spacer script.
