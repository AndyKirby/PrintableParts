# A Micro-switch Mounting Bracket

![Hardware Photo](MSwitchMnt90.jpg)

This is a bracket for mounting micro-switches to M8 aluminium extruded machine framing. Useful for implementing end stop's, homing switches and guard switches. The holes for bolting the micro-switch are implemented as slots to allow for adjusting the switching position.

![CAD Photo](CADMSwitchMnt90.png)
