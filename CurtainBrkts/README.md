# A Pair of curtain hanging brackets.

![Hardware Photo](Fitted.jpg)

These are designed to be used with a curtain pole made from 20mm electrical conduit. Choice of curtian material is your own I used clear shower curtain.

The brackets and a clear curtain assembly are used on my 3D printing bay to keep the fumes down (There is an air scrubber inside) and keep dust off the printer when it is inactive. Enclosing the prining bay also helps stabilise the ambient temperature and reduce the cooling drafts that make a mess of ABS printing. The channel on the brackets is made deeper than needed so that the curtains lower pole can hang up there to give access to the printer bay when settign up and retrieving prints.