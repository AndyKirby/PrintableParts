# A case for an electronic meter probe

![Hardware Photo](MeterProbe.jpg)

The meter probe is a case to take an IR meter reader circuit so it can be secured to the optical reader port on the front of modern electronic meters. The sort of meter commonly used for a domestic electricity supply. This was being created as part of a home automation and monitoring project so that electricity usage could be logged and analysed.

![Reader Body Photo](ReaderBody.jpg)

the reader body has a cut-out to allow cable entry and a pair of ports in the underside that line up with the IR receiver and red LED that make up the meters reader port. The smaller of the two holes is sized to take an IR LED to send data. The larger of the holes is sized so that a [BPW34](http://www.vishay.com/docs/81521/bpw34.pdf) pin diode will sit in side the hole, screening it from IR produced by the readers LED.

![Reader Lid Photo](ReaderLid.jpg)

A lid was created that is a tight snap on fit. The snap on action relies on the surface ridges characteristic of 3D printing to hold it in place. Glue could also be used.

![Print Set Full Reader Case](FullreaderCase.jpg)

A full reader case pritn set was created to print out a whole case in one go.

# Notes:-
* Although the initial picture shows a case printed in natural ABS this was not light tight enough, changing the printed to material to black ABS produced better results.
* Secure the reader to the meter face using blue tack or dabs of easily removed rubber solution glue
