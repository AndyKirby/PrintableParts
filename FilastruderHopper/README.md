# Filastruder replacement horizontal hopper

![Hardware Photo](FilastruderHopper.jpeg)

This is a replacement hopper for the filastruder kit. The one that came with it was terrible as it would not stay put, was made out of low temperature plastic (PLA), too small and impossible to remove without disassembling the entire unit if there were problems. This design fixed most of those issues. The design was based on critical dimensions taken from a completed unit.

![Dims Sketch](DimsSketch.jpg)

The design comes in 3 parts.  The funnel which is a press fit but could be solvent welded to the hopper body if you never needed to change it again.

![Funnel](FunlHorz.jpeg)

The hopper top which the funnel fits on to.

![HTop](HopHTop.jpeg)

The hopper bottom. The hopper top and bottom are bolted together around the filastruder pipe clamping it securely in place and allowing to be removed if needs be without disassembly of the filastruder itself.

![HBottom](HopHBot.jpeg)
