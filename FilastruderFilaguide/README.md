# Filastruder filament guide

![Hardware Photo](FillaGuide.jpg)

This is a replacement filament guide to use in place of the one supplied with the filastruder. This design uses 2 of each part and a skate board bearing (608XZZ) to make up the assembly. Print two tyres and press them into place on the bearing, flange outwards.

![Tyre Photo](608Tyre.png)

Print two brackets and mount them back to back on the filastruder in place of the original filament guide. Use an M8 bolt, washers and nyloc nut as the axle to fit your previously constructed guide wheel to the brackets. Ensure it turns freely.

![Bracket Photo](Bracket.png)
