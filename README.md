PrintableParts
==============

Designs for Mechanical Parts created with 3D Printing in mind.
Earlier parts were scripted using [OpenScad *.scad](http://www.openscad.org/) but most are now drawn using [Freecad *.fcstd](http://www.freecadweb.org/)

Explanations can be found under each folder along with pictures and the files that are necessary to make the parts.

