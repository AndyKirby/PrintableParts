# A blood or drainage bag hook

![Hardware Photo](BagHook.png)

This is a hook to hang a blood or drainage bag over a dispensing machine. The idea is that gravity will help feed the viscous liquids from the bag being used as a reservoir and being in a bag allows the liquid to flow but stops the contents being exposed to the air or drying out.



