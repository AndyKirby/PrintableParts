# Spring Seats

![Hardware Photo](SpringSeat.jpg)

Files for generating spring seats to centre tensioning springs and help them work better. The photo above is of some I turned on my mini-lathe whilst building the printer that can print the parts in these files. There is nothing worse functionally than tensioning springs mounted without seats to keep them centred and stop them catching on the tensioning bolt. Keeping the springs free to do their job gives reliable and repeatable results. When printing these in plastic, back the seat with a correctly sized washer or the plastic will creep over time.

![CAD Photo](SpringSeatD.jpg)

Some pre generated spring seat files for a single seat and for 9 seats can also be found here, These are sized for the tensioning springs and fixing bolts on a Reprap Wades Extruder.
