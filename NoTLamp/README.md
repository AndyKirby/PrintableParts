A WiFi sensor cluster for a home automation NoT (Network of things).

![Hardware Photo](NoTLampAssembly.jpg)

The cluster is buit as a lamp and functions as an automatic lamp. It is mains powered and monitors the home environment, reporting it's state via MQTT (Mosquitto). Using MQTT as a middle layer seperates out the NoT from the command, control and overview.

This is the 3D CAD design for the printable parts of the lamp and an assembly to show how they are put together.

The Lamp comprises a shade, base, pole, top and diffuser disk. The base and Top are 3D printable, the pole is made from common sink waste pipe. The shade and diffuser disk are made from lamp shade stiffener plastic covered in a material or pattern to match your decor. The shade covers the majority of the lamp desiguising the contents and leavign it to be deployed unobtrusively in the household on a one per room basis.

![CAD Photo Base](NoTLampBase.jpg)

![CAD Photo Top](NoTLampTop.jpg)

This enclosure design is intended to be used with the following components:-

* Firmware, https://github.com/AndyKirby/Firmware/tree/master/NoTLamp
* Electronics, https://github.com/AndyKirby/Electronics/tree/master/NoTLamp

The lamp shade is dimensioned to be cylinder just under A3 in size, whilst the diffuser disk is a disk 107mm in diameter of the same material.

The design was realised using FOSS throughout. Drawings and parts design via Freecad and the printed parts were printed on a Mendel90 managed through octoprint after being exported as .stl files from Freecad and sliced using Skeinforge. 

* Freecad, http://www.freecadweb.org/
* Skeinforge, http://reprap.org/wiki/Skeinforge
* Mendel90, http://reprap.org/wiki/Mendel90
* Octoprint, http://octoprint.org/
