# A RAMPS Board Mounting Bracket

![Hardware Photo](RampsMnt.png)

This is a bracket for mounting an Arduino RAMPS board combo to M8 slotted, extruded aluminium, machine framing. This was designed for using off the shelf RAMPS boards as a basic CNC machine controller. The bracket features mounting for the cooling fan, board mounting points with integral stand-off's and cable management.

![CAD Photo](CADRampsMnt.png)
